package com.rutingtech.evaluator.controller;

import com.rutingtech.evaluator.domain.entity.OutputIndex;
import com.rutingtech.evaluator.service.OutputService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class OutputController {

    @Resource
    private OutputService outputService;

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public List<OutputIndex> getAllIndex() {
        return outputService.getAll();
    }
}
