package com.rutingtech.evaluator.service;

import com.rutingtech.evaluator.domain.entity.OutputIndex;
import com.rutingtech.evaluator.domain.mapper.OutputIndexMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OutputService {

    @Resource
    private OutputIndexMapper outputIndexMapper;

    public List<OutputIndex> getAll() {
        return outputIndexMapper.getAll();
    }
}
