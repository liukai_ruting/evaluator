package com.rutingtech.evaluator.domain.mapper;

import com.rutingtech.evaluator.domain.entity.OutputIndex;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OutputIndexMapper {

    List<OutputIndex> getAll();
}
