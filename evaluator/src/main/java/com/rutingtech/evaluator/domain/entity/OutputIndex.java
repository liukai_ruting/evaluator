package com.rutingtech.evaluator.domain.entity;

public class OutputIndex {

    private String id;

    private String name;

    private double weight;

    private double expect;

//    private String evaluateType;
    private String evaluate_type;

    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getExpect() {
        return expect;
    }

    public String getEvaluate_type() {
        return evaluate_type;
    }

    public void setEvaluate_type(String evaluate_type) {
        this.evaluate_type = evaluate_type;
    }

    public void setExpect(double expect) {
        this.expect = expect;
    }

//    public String getEvaluateType() {
//        return evaluateType;
//    }
//
//    public void setEvaluateType(String evaluateType) {
//        this.evaluateType = evaluateType;
//    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
