package com.rutingtech.evaluator.service;

import com.rutingtech.evaluator.Application;
import com.rutingtech.evaluator.domain.entity.OutputIndex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class OutputIndexServiceTest {

    @Resource
    private OutputService outputService;

    @Test
    public void getAllTest() {
        List<OutputIndex> outputIndex = outputService.getAll();
    }
}
