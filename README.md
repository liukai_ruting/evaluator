# evaluator

### 项目构建

- 过程：由IDEA创建空项目。

- 开发环境
    - IDE：IntelliJ IDEA 2017.2
    - OS：Windows 10/64bit

### 模块(Module)

- evaluator
    - 描述：
    
        相关接口开发。
        
    - 过程：由IDEA创建Maven模块
        
    - 开发环境
        - Web：Spring Boot
        - ROM：Mybatis
        - DB：MySQL 5.7.18
        - 构建工具：Maven